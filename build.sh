set -xe

# Get version from package.json
VERSION=$(cat package.json | grep version | head -1 | awk -F: '{ print $2 }' | sed 's/[", ]//g')
echo "Building version ${VERSION}"

docker build -t harbor.k8s.inpt.fr/net7/edt-tls-secte:${VERSION} .
docker push harbor.k8s.inpt.fr/net7/edt-tls-secte:${VERSION}