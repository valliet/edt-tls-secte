import express from "express";
import ical from "node-ical";
import ics from "ics";

import dotenv from "dotenv";
dotenv.config();

const EDT_ENSEEIHT=process.env.EDT_ENSEEIHT;
const EDT_INSA=process.env.EDT_INSA;

const app = express();

app.get('/', async function (req, res) {
  let icalEnseeiht, icalInsa;
  try {
    icalEnseeiht = await ical.async.fromURL(EDT_ENSEEIHT);
    icalInsa = await ical.async.fromURL(EDT_INSA);

    // If either of the ical files is empty, we return an error
    if (!icalEnseeiht || !icalInsa) {
      console.error("One of the ical files is empty");
      res.status(500).send("Can't fetch ical files");
      return;
    }

    // For each event in icalEnseeiht, we add it to mergedEvents
    const mergedEvents = {};
    for (const event of Object.values(icalEnseeiht)) {
      if (event.type !== "VEVENT") continue;

      const start = new Date(event.start);
      const end = new Date(event.end);
      const created = new Date(event.created);

      if (event.location === "INSA") {
        // Find the corresponding event in icalEnsat
        const correspondingEvent = Object.values(icalInsa).find(
          // Two events are considered to be the same if they are colliding
          // (i.e. they may not have the same start and end dates)
          (e) => {
            const insaStart = new Date(e.start);
            const insaEnd = new Date(e.end);

            return (
              e.type === "VEVENT" &&
              new Date(Math.max(start, insaStart)) < new Date(Math.min(end, insaEnd))
            );
          }
        );

        if (correspondingEvent) {
          event.location = correspondingEvent.location;
        };
      }

      // Remove keys that are not needed by ics
      delete event.type;
      delete event.params;
      delete event.dtstamp;
      delete event.datetype;
      delete event.lastmodified;

      mergedEvents[event.uid] = {
        ...event,
        sequence: parseInt(event.sequence),
        title: event.summary,
        start: [start.getFullYear(), start.getMonth()+1, start.getDate(), start.getHours(), start.getMinutes()],
        end: [end.getFullYear(), end.getMonth()+1, end.getDate(), end.getHours(), end.getMinutes()],
        created: [created.getFullYear(), created.getMonth()+1, created.getDate(), created.getHours(), created.getMinutes()],
      };
    }

    // Return the merged events as an ics file
    const { error, value } = ics.createEvents(Object.values(mergedEvents));
    if (error) {
      console.error(error);
      res.status(500).send("Can't create ics file");
    } else {
      res.set("Content-Type", "text/calendar");
      res.set("Content-Disposition", "attachment; filename=edt-tls-secte.ics");
      res.send(value);
    }

  } catch (e) {
    console.error(e);
    res.status(500).send("Internal server error");
  }
});
    

app.listen(3000, () => {
  console.info("Server running on http://localhost:3000");
  }
);